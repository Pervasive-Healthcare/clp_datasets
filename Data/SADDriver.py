import os
from Utils.driver import Driver
import pickle

POSITIONS = ["left pocket", "right pocket", "wrist", "upper arm", "belt"]
DEVICE = "Galaxy SII (i9100)"


def get_array_of_obj_xyz(data):
    array = []
    for single_data in data:
        values = single_data.replace("\n", "").split(" ")
        obj = {"x": float(values[0]), "y": float(values[1]), "z": float(values[2])}
        array.append(obj)
    return array


class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "SAD"

    gyro_data = []
    acc_data = []
    magne_data = []
    acc_lin_data = []

    def create_json(
        self, label_id, position, gyro_data, acc_data, magne_data, acc_lin_data
    ):
        single_data = {
            "label": label_id,
            "position": position,
            "device": DEVICE,
            "gyroscope": get_array_of_obj_xyz(gyro_data),
            "accelerometer": get_array_of_obj_xyz(acc_data),
            "magnetometer": get_array_of_obj_xyz(magne_data),
        }
        self.data.append(single_data)

    def save_data(self, prev_activity):
        x = 0
        y = 1
        z = 2
        for position in POSITIONS:
            self.create_json(
                prev_activity,
                position,
                [
                    item.split(" ")[x]
                    + " "
                    + item.split(" ")[y]
                    + " "
                    + item.split(" ")[z]
                    for item in self.gyro_data
                ],
                [
                    item.split(" ")[x]
                    + " "
                    + item.split(" ")[y]
                    + " "
                    + item.split(" ")[z]
                    for item in self.acc_data
                ],
                [
                    item.split(" ")[x]
                    + " "
                    + item.split(" ")[y]
                    + " "
                    + item.split(" ")[z]
                    for item in self.magne_data
                ],
                [
                    item.split(" ")[x]
                    + " "
                    + item.split(" ")[y]
                    + " "
                    + item.split(" ")[z]
                    for item in self.acc_lin_data
                ],
            )
            x = x + 3
            y = y + 3
            z = z + 3
        self.gyro_data.clear()
        self.acc_data.clear()
        self.magne_data.clear()
        self.acc_lin_data.clear()

    def read(self, path):
        path = os.path.join(path, "SAD", "")
        os.chdir(path)

        config = self.get_config()

        try:
            with open('data_SAD', 'rb') as f:
                self.data = pickle.load(f)
            
            print("data_SAD found, pickle loaded.")

            return self.data, config
        except:
            pass

        for _, _, files in os.walk(os.getcwd()):
            for file in files:
                if file.endswith(".csv"):
                    file = open(file, "r")
                    rows = file.readlines()
                    rows = rows[2:]
                    prev_activity = rows[0].split(",")[69].replace("\n", "")
                    for row in rows:
                        fields = row.split(",")
                        current_activity = fields[len(fields) - 1].replace("\n", "")
                        if prev_activity != current_activity:
                            self.save_data(prev_activity)
                            prev_activity = current_activity
                        self.acc_data.append(
                            fields[1]
                            + " "
                            + fields[2]
                            + " "
                            + fields[3]
                            + " "
                            + fields[15]
                            + " "
                            + fields[16]
                            + " "
                            + fields[17]
                            + " "
                            + fields[29]
                            + " "
                            + fields[30]
                            + " "
                            + fields[31]
                            + " "
                            + fields[43]
                            + " "
                            + fields[44]
                            + " "
                            + fields[45]
                            + " "
                            + fields[57]
                            + " "
                            + fields[58]
                            + " "
                            + fields[59]
                        )
                        self.acc_lin_data.append( 
                            fields[4]
                            + " "
                            + fields[5]
                            + " "
                            + fields[6]
                            + " "
                            + fields[18]
                            + " "
                            + fields[19]
                            + " "
                            + fields[20]
                            + " "
                            + fields[32]
                            + " "
                            + fields[33]
                            + " "
                            + fields[34]
                            + " "
                            + fields[46]
                            + " "
                            + fields[47]
                            + " "
                            + fields[48]
                            + " "
                            + fields[60]
                            + " "
                            + fields[61]
                            + " "
                            + fields[62]
                        )
                        self.gyro_data.append(
                            fields[7]
                            + " "
                            + fields[8]
                            + " "
                            + fields[9]
                            + " "
                            + fields[21]
                            + " "
                            + fields[22]
                            + " "
                            + fields[23]
                            + " "
                            + fields[35]
                            + " "
                            + fields[36]
                            + " "
                            + fields[37]
                            + " "
                            + fields[49]
                            + " "
                            + fields[50]
                            + " "
                            + fields[51]
                            + " "
                            + fields[63]
                            + " "
                            + fields[64]
                            + " "
                            + fields[65]
                        )
                        self.magne_data.append(
                            fields[10]
                            + " "
                            + fields[11]
                            + " "
                            + fields[12]
                            + " "
                            + fields[24]
                            + " "
                            + fields[25]
                            + " "
                            + fields[26]
                            + " "
                            + fields[38]
                            + " "
                            + fields[39]
                            + " "
                            + fields[40]
                            + " "
                            + fields[52]
                            + " "
                            + fields[53]
                            + " "
                            + fields[54]
                            + " "
                            + fields[66]
                            + " "
                            + fields[67]
                            + " "
                            + fields[68]
                        )
                    self.save_data(current_activity)

        with open('data_SAD', 'wb') as f:
            pickle.dump(self.data,f)
        print("Data SAD saved on pickle!")

        return self.data, config
