import os
from Utils.driver import Driver

sensors = ["Phones_accelerometer", "Phones_gyroscope", "Watch_accelerometer", "Watch_gyroscope"]
acc_data = {}

def get_array_of_obj_xyz(key):
    array = []
    for values in acc_data[key][3:]:
        obj = {"x": float(values[0]), "y": float(values[1]), "z": float(values[2])}
        array.append(obj)
    return array

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "HHAR"

    def create_json(self, key):
        single_data = {
            "label": acc_data[key][1],
            "device": acc_data[key][0],
            "accelerometer": get_array_of_obj_xyz(key)
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "HHAR")
        os.chdir(path)

        config = self.get_config()

        for sensor in sensors:
            file = open(sensor + ".csv", "r")
            acc_data.clear()
            for line in file:
                ln = line.replace("\n", "").split(",")
                acc_value = [ln[3], ln[4], ln[5]]

                if ln[8] + ln[9] in acc_data:
                    acc_data[ln[8]+ln[9]].append(acc_value)
                else:
                    new_data = [ln[8], ln[9]]
                    acc_data[ln[8]+ln[9]] = new_data
                    acc_data[ln[8]+ln[9]].append(acc_value)

            for key in acc_data:
                if key != "Devicegt":
                    self.create_json(key)

        return self.data, config
