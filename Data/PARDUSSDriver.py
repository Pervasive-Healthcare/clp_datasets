import os
import pandas as pd
from Utils.driver import Driver

positions = ["Arm", "Belt", "Pocket", "Wrist"]

def get_array_of_obj_xyz(data):
    array = []
    for values in data:
        obj = {"timestamp": float(values[0]), "x": float(values[1]), "y": float(values[2]), "z": float(values[3])}
        array.append(obj)
    return array

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "PARDUSS"

    def create_json(self, activity, position, accelerometer, gyroscope, magnetometer):
        single_data = {
            "label" : activity,
            "position" : position,
            "device" : "Samsung Galaxy S2",
            "accelerometer": get_array_of_obj_xyz(accelerometer),
            "gyroscope": get_array_of_obj_xyz(gyroscope),
            "magnetometer": get_array_of_obj_xyz(magnetometer)
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "PARDUSS")
        os.chdir(path)

        config = self.get_config()

        for position in positions:
            file = pd.read_excel(position + ".xlsx", sheet_name = None)["Blad1"]
            acc_data = []
            gyro_data = []
            mag_data = []
            prev_activity = file.iloc[0][10]
            for row in range(0, len(file)):

                curr_activity = file.iloc[row][10]

                if curr_activity != prev_activity:
                    self.create_json(prev_activity, position, acc_data, gyro_data, mag_data)
                    acc_data = []
                    gyro_data = []
                    mag_data = []

                acc = [file.iloc[row][0], file.iloc[row][1], file.iloc[row][2], file.iloc[row][3]]
                gyro = [file.iloc[row][0], file.iloc[row][4], file.iloc[row][5], file.iloc[row][6]]
                mag = [file.iloc[row][0], file.iloc[row][7], file.iloc[row][8], file.iloc[row][9]]
                acc_data.append(acc)
                gyro_data.append(gyro)
                mag_data.append(mag)

                if row == (len(file)-1):
                    self.create_json(prev_activity, position, acc_data, gyro_data, mag_data)
                    acc_data = []
                    gyro_data = []
                    mag_data = []

                prev_activity = file.iloc[row][10]

        return self.data, config
