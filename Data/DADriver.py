import os
from Utils.driver import Driver

def get_array_of_obj_xyz(data):
    array = []
    for values in data[1:]:
        obj = {"x": float(values[0]), "y": float(values[1]), "z": float(values[2])}
        array.append(obj)
    return array

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "DA"

    def create_json(self, activity, accelerometer):
        single_data = {
            "label": activity,
            "position": "front pocket",
            "device" : "Nokia N8",
            "accelerometer": get_array_of_obj_xyz(accelerometer)
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "DA")
        os.chdir(path)

        config = self.get_config()

        for i in range(1, 9):

            idling_data = []
            walking_data = []
            cycling_data = []
            driving_data = []
            running_data = []

            file = open("subject" + f'{i}' + ".txt", "r").readlines()

            for line in file:
                values = line.replace("\n", "").split(",")
                activity = values[3]
                if activity == "1":
                    idling_data.append(values)
                if activity == "2":
                    walking_data.append(values)
                if activity == "5":
                    cycling_data.append(values)
                if activity == "6":
                    driving_data.append(values)
                if activity == "7":
                    running_data.append(values)

            if len(idling_data) != 0:
                self.create_json("idling", idling_data)
            if len(walking_data) != 0:
                self.create_json("walking", walking_data)
            if len(cycling_data) != 0:
                self.create_json("cycling", cycling_data)
            if len(driving_data) != 0:
                self.create_json("driving", driving_data)
            if len(running_data) != 0:
                self.create_json("running", running_data)

        return self.data, config
