import os
import pandas as pd
from Utils.driver import Driver
import pickle
import joblib

map_activity = {
    "STD": "standing",
    "WAL": "walking",
    "JOG": "jogging",
    "JUM": "jumping",
    "STU": "stairs up",
    "STN": "stairs down",
    "SCH": "sit chair",
    "CSI": "car step in",
    "CSO": "car step out",
    "FOL": "forward lying",
    "FKL": "front knees lying",
    "BSC": "back sitting chair",
    "SDL": "sideward lying",
}


def get_user_obj(user):
    sex = "M"
    if user.loc[6, "value"] == "Female":
        sex = "F"
    obj = {
        "id": int(user.loc[0, "value"]),
        "age": int(user.loc[3, "value"]),
        "height": int(user.loc[4, "value"]),
        "weight": int(user.loc[5, "value"]),
        "sex": sex,
    }
    return obj



# <ADL OR FALL_CODE>_<SENSOR_CODE>_<SUBJECT_ID>_<TRIAL_NO>.txt
class DriverImplementation(Driver):
    data = []

    def create_json(self, current_activity, current_user, current_exp, acc, gyro, ori):

        acc.loc[:, 'timestamp'] = pd.to_numeric(acc.loc[:, 'timestamp'].copy())
        gyro.loc[:, 'timestamp'] = pd.to_numeric(gyro.loc[:, "timestamp"].copy())
        ori.loc[:, 'timestamp'] = pd.to_numeric(ori.loc[:, "timestamp"].copy())

        single_data = {
            "label": map_activity[current_activity],
            "user": get_user_obj(current_user),
            "accelerometer": list(acc.T.to_dict().values()),
            "gyroscope": list(gyro.T.to_dict().values()),
            "orientation": list(ori.T.to_dict().values()),
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "MobiAct/MobiAct_Dataset")
        os.chdir(path)

        config = self.get_config()
        
        try:
            with open('data_Mobiact.pkl', 'rb') as f:
                self.data = pickle.load(f)
                
            print("data_Mobiact found, pickle loaded.")

            return self.data, config
        except Exception as valerr:
            print(valerr)
            pass

        # try:
        #     filename = 'data_Mobiact2'
        #     self.data = joblib.load(filename)

        #     print("data_Mobiact2 found, joblib loaded.")

        #     return self.data, config
        # except Exception as valerr:
        #     print(valerr)
        #     pass

        a = 0
        for root, dirs, files in os.walk(os.getcwd()):
            current_activity = root[-3:]
            print(current_activity + "...")
            for file in files:
                if file.endswith(".txt") and "_acc" in file:
                    #print(a)
                    a = a+1
                    file_info = file.split("_")
                    current_exp = file_info[3].replace(".txt", "")
                    user_info = pd.read_csv(
                        current_activity + "/" + file,
                        skiprows=5,
                        nrows=7,
                        sep=":",
                        names=["key", "value"],
                    )
                    acc = pd.read_csv(
                        current_activity + "/" + file,
                        names=["timestamp", "x", "y", "z"],
                        skiprows=15,
                        sep=",",
                    )
                    gyro = pd.read_csv(
                        current_activity + "/" + file.replace("acc", "gyro"),
                        names=["timestamp", "x", "y", "z"],
                        skiprows=15,
                        sep=",",
                    )
                    ori = pd.read_csv(
                        current_activity + "/" + file.replace("acc", "ori"),
                        names=["timestamp", "x", "y", "z"],
                        skiprows=15,
                        sep=",",
                    )
                    self.create_json(current_activity, user_info, current_exp, acc.iloc[1:], gyro.iloc[1:], ori.iloc[1:])
        
        try:
            with open('data_Mobiact.pkl', 'wb') as f:
                pickle.dump(self.data,f,protocol=4)
            print("Data MobiAct saved on pickle!")
        except:
            pass

        # try:
        #     filename = 'data_Mobiact2'
        #     joblib.dump(self.data, filename)  
        #     print("Data MobiAct saved with joblib!")
        # except:
        #     pass

        return self.data, config

    def get_dataset_name(self):
        return "MobiAct"