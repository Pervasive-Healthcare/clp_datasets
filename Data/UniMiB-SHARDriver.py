import os
from scipy import io
from Utils.driver import Driver

map_activity = {
    0: "Walking",
    1: "Running",
    2: "GoingUpS",
    3: "GoingDownS",
    4: "Jumping",
    5: "SittingDown",
    6: "StandingUpFS",
    7: "LyingDownFS",
    8: "StandingUpFL",
    9: "FallingForw",
    10: "FallingBack",
    11: "FallingRight",
    12: "FallingLeft",
    13: "Syncope",
    14: "FallingBackSC",
    15: "FallingWithPS",
    16: "HittingObstacle"
}

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "UniMiB"

    def create_json(self, activity, subject, position, accelerometer):
        single_data = {
            "label": activity,
            "user": subject,
            "position": position,
            "device": "Samsung Galaxy Nexus I9250",
            "accelerometer": accelerometer
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "UniMiB-SHAR")
        os.chdir(path)

        config = self.get_config()

        arr = io.loadmat('data/full_data.mat')['full_data']

        for a in range(0, len(arr)):
            subject = {
                "id": a+1,
                "age": int(arr[a][2][0][0]),
                "height": int(arr[a][3][0][0]),
                "weight": int(arr[a][4][0][0]),
                "sex": arr[a][1][0][0],
            }
            for b in range(0, len(arr[a][0][0][0])): #for each activity
                for c in range(0, len(arr[a][0][0][0][b])): #for each trial

                    if len(arr[a][0][0][0][b]) == 2:
                        if c == 0:
                            position = "right pocket"
                        else:
                            position = "left pocket"
                    if len(arr[a][0][0][0][b]) == 6:
                        if c < 3:
                            position = "right pocket"
                        elif c >= 3:
                            position = "left pocket"

                    acc_data = []
                    for d in range(0, len(arr[a][0][0][0][b][c][0][0])):
                        obj = {
                            "x": float(arr[a][0][0][0][b][c][0][0][d]),
                            "y": float(arr[a][0][0][0][b][c][0][1][d]),
                            "z": float(arr[a][0][0][0][b][c][0][2][d])
                        }
                        acc_data.append(obj)
                    self.create_json(map_activity[b], subject, position, acc_data)

        return self.data, config
