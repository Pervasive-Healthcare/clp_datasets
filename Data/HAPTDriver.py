import os
from Utils.driver import Driver
import pickle

# http://127.0.0.1:5000/import/UciHAPT

map_activity = {
    1: "WALKING",
    2: "WALKING_UPSTAIRS",
    3: "WALKING_DOWNSTAIRS",
    4: "SITTING",
    5: "STANDING",
    6: "LAYING",
    7: "STAND_TO_SIT",
    8: "SIT_TO_STAND",
    9: "SIT_TO_LIE",
    10: "LIE_TO_SIT",
    11: "STAND_TO_LIE",
    12: "LIE_TO_STAND",
}


def get_array_of_obj_xyz(data):
    array = []
    for single_data in data:
        values = single_data.replace("\n", "").split(" ")
        obj = {"x": float(values[0]), "y": float(values[1]), "z": float(values[2])}
        array.append(obj)
    return array


class DriverImplementation(Driver):
    data = []

    def create_json(self, label_id, data_from, data_to, gyro_data, acc_data):
        gyroscope = gyro_data[int(data_from) - 1 : int(data_to) - 1]
        accelerometer = acc_data[int(data_from) - 1 : int(data_to) - 1]
        single_data = {
            "label": map_activity[label_id].replace("_", " ").lower(),
            "gyroscope": get_array_of_obj_xyz(gyroscope),
            "accelerometer": get_array_of_obj_xyz(accelerometer),
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "HAPT/RawData", "")
        os.chdir(path)

        config = self.get_config()

        try:
            
            with open('data_HAPT', 'rb') as f:
                self.data = pickle.load(f)
            
            print("data_HAPT found, pickle loaded.")

            return self.data, config
        except:
            pass

        labels_file = open("labels.txt", "r")
        labels = labels_file.readlines()

        # init variables
        prev_experiment_id = -1
        prev_user_id = -1
        gyro_data = []
        acc_data = []
        for label in labels:
            fields = label.replace("-", "").split(" ")
            # set info read from a single line of labels.txt
            experiment_id = fields[0]
            user_id = fields[1]
            label_id = int(fields[2])
            data_from = fields[3]
            data_to = fields[4]
            # verify if user and experiment are the same respect previous iteration
            # if not open the respective two files with current user and experiment data
            if experiment_id != prev_experiment_id and prev_user_id != user_id:
                gyro_file = open(
                    "gyro_exp"
                    + experiment_id.zfill(2)
                    + "_user"
                    + user_id.zfill(2)
                    + ".txt",
                    "r",
                )
                acc_file = open(
                    "gyro_exp"
                    + experiment_id.zfill(2)
                    + "_user"
                    + user_id.zfill(2)
                    + ".txt",
                    "r",
                )
                gyro_data = gyro_file.readlines()
                acc_data = acc_file.readlines()
                gyro_file.close()
                acc_file.close()
            # create a document for current user, experiment and activity
            self.create_json(label_id, data_from, data_to, gyro_data, acc_data)
            # update user and experiment variable
            prev_experiment_id = experiment_id
            prev_user_id = user_id

        with open('data_HAPT', 'wb') as f:
            pickle.dump(self.data,f)
        print("Data HAPT saved on pickle!")
        
        return self.data, config

    def get_dataset_name(self):
        return "UciHAPT"
