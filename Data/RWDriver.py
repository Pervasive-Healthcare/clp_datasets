import os
import zipfile
from io import BytesIO
from Utils.driver import Driver

subject = {
    1 : {"Gender" : "Female", "Age" : 52, "Height" : 163, "Weight" : 48},
    2 : {"Gender" : "Male", "Age" : 26, "Height" : 179, "Weight" : 70},
    3 : {"Gender" : "Male", "Age" : 27, "Height" : 176, "Weight" : 81},
    4 : {"Gender" : "Male", "Age" : 26, "Height" : 183, "Weight" : 82},
    5 : {"Gender" : "Male", "Age" : 62, "Height" : 170, "Weight" : 70},
    6 : {"Gender" : "Female", "Age" : 24, "Height" : 174, "Weight" : 65},
    7 : {"Gender" : "Male", "Age" : 26, "Height" : 180, "Weight" : 81},
    8 : {"Gender" : "Female", "Age" : 36, "Height" : 165, "Weight" : 95},
    9 : {"Gender" : "Male", "Age" : 26, "Height" : 179, "Weight" : 95},
    10 : {"Gender" : "Male", "Age" : 26, "Height" : 170, "Weight" : 90},
    11 : {"Gender" : "Female", "Age" : 48, "Height" : 175, "Weight" : 71},
    12 : {"Gender" : "Female", "Age" : 16, "Height" : 164, "Weight" : 54},
    13 : {"Gender" : "Female", "Age" : 27, "Height" : 170, "Weight" : 65},
    14 : {"Gender" : "Male", "Age" : 26, "Height" : 183, "Weight" : 78},
    15 : {"Gender" : "Female", "Age" : 30, "Height" : 165, "Weight" : 66}
}
positions = [
    "chest",
    "forearm",
    "head",
    "shin",
    "thigh",
    "upperarm",
    "waist"
]
activities = [
	"climbingdown",
    "climbingup",
    "jumping",
    "lying",
	"running",
	"sitting",
    "standing",
    "walking"
]

def get_user_obj(user):
    obj = {
        "id": user,
        "age": subject[user]["Age"],
        "height": subject[user]["Height"],
        "weight": subject[user]["Weight"],
        "sex": subject[user]["Gender"],
    }
    return obj

def get_array_of_obj_xyz(data):
    array = []
    for single_data in data[1:]:
        values = single_data.decode('utf-8').replace("\n", "").split(",")
        obj = {"timestamp": float(values[1]),"x": float(values[2]), "y": float(values[3]), "z": float(values[4])}
        array.append(obj)
    return array

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "RW"

    def create_json(self, activity, subject, position, accelerometer, gyroscope, magnetometer):
        single_data = {
            "label": activity,
            "user": get_user_obj(subject),
            "position": position,
            "accelerometer": get_array_of_obj_xyz(accelerometer),
            "gyroscope": get_array_of_obj_xyz(gyroscope),
            "magnetometer": get_array_of_obj_xyz(magnetometer)
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "RW")
        os.chdir(path)

        config = self.get_config()

        # you may consider opening one folder at time like this:
        # for i in range(1, 2), for i in range(2, 3) ...
        for i in range(1, 16):

            for act in activities:

                acc_file = zipfile.ZipFile('proband' + f'{i}' + '/data/acc_' + act + '_csv.zip')
                gyr_file = zipfile.ZipFile('proband' + f'{i}' + '/data/gyr_' + act + '_csv.zip')
                mag_file = zipfile.ZipFile('proband' + f'{i}' + '/data/mag_' + act + '_csv.zip')

                # .zip within a .zip
                if (i == 4 or i == 7 or i == 14) and (act == "climbingup" or act == "climbingdown"):
                    for n in range(1, 4):
                        if n == 1:
                            zip_acc = zipfile.ZipFile(BytesIO(acc_file.read('acc_' + act + '_1_csv.zip')))
                            zip_gyr = zipfile.ZipFile(BytesIO(gyr_file.read('gyr_' + act + '_csv.zip')))
                            zip_mag = zipfile.ZipFile(BytesIO(mag_file.read('mag_' + act + '_csv.zip')))

                            for pos in positions:
                                acc_data = zip_acc.open('acc_' + act + '_' + pos + '.csv').readlines()
                                gyro_data = zip_gyr.open('Gyroscope_' + act + '_' + pos + '.csv').readlines()
                                mag_data = zip_mag.open('MagneticField_' + act + '_' + pos + '.csv').readlines()
                                self.create_json(act, i, pos, acc_data, gyro_data, mag_data)

                        else:
                            zip_acc = zipfile.ZipFile(BytesIO(acc_file.read('acc_' + act + '_' + f'{n}' + '_csv.zip')))
                            zip_gyr = zipfile.ZipFile(BytesIO(gyr_file.read('gyr_' + act + '_' + f'{n}' + '_csv.zip')))
                            zip_mag = zipfile.ZipFile(BytesIO(mag_file.read('mag_' + act + '_' + f'{n}' + '_csv.zip')))

                            for pos in positions:
                                acc_data = zip_acc.open('acc_' + act + '_' + f'{n}' + '_' + pos + '.csv').readlines()
                                gyro_data = zip_gyr.open('Gyroscope_' + act + '_' + f'{n}' + '_' + pos + '.csv').readlines()
                                mag_data = zip_mag.open('MagneticField_' + act + '_' + f'{n}' + '_' + pos + '.csv').readlines()
                                self.create_json(act, i, pos, acc_data, gyro_data, mag_data)

                        zip_acc.close()
                        zip_gyr.close()
                        zip_mag.close()

                else:
                    for pos in positions:
                        # missing files
                        if ((i == 2 and act == "climbingup" and pos == "forearm") or
                            (i == 6 and act == "jumping" and pos == "thigh")):
                            continue

                        # unusual file names
                        if (((i == 4 or i == 13) and act == "walking") or
                            ((i == 6 or i == 7) and act == "sitting") or
                            (i == 8 and act == "standing")):
                            acc_data = acc_file.open('acc_' + act + '_2_' + pos + '.csv').readlines()
                            gyro_data = gyr_file.open('Gyroscope_' + act + '_2_' + pos + '.csv').readlines()
                            mag_data = mag_file.open('MagneticField_' + act + '_2_' + pos + '.csv').readlines()
                        else:
                            acc_data = acc_file.open('acc_' + act + '_' + pos + '.csv').readlines()
                            gyro_data = gyr_file.open('Gyroscope_' + act + '_' + pos + '.csv').readlines()
                            mag_data = mag_file.open('MagneticField_' + act + '_' + pos + '.csv').readlines()
                        self.create_json(act, i, pos, acc_data, gyro_data, mag_data)

                    acc_file.close()
                    gyr_file.close()
                    mag_file.close()

        return self.data, config
