import os
import zipfile
from Utils.driver import Driver

activities = ["dws_1", "dws_2", "dws_11", "jog_9", "jog_16", "sit_5", "sit_13", "std_6", "std_14", "ups_3", "ups_4", "ups_12", "wlk_7", "wlk_8", "wlk_15"]
subjects = []
map_activity = {
    "dws_1": "dws",
    "dws_2": "dws",
    "dws_11": "dws",
    "jog_9": "jog",
    "jog_16": "jog",
    "sit_5": "sit",
    "sit_13": "sit",
    "std_6": "std",
    "std_14": "std",
    "ups_3": "ups",
    "ups_4": "ups",
    "ups_12": "ups",
    "wlk_7": "wlk",
    "wlk_8": "wlk",
    "wlk_15": "wlk"
}

def get_array_of_obj_xyz(data):
    array = []
    for single_data in data[1:]:
        values = single_data.decode('utf-8').replace("\n", "").split(",")
        obj = {"x": float(values[1]), "y": float(values[2]), "z": float(values[3])}
        array.append(obj)
    return array

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "MS"

    def create_json(self, activity, user, accelerometer, gyroscope):
        single_data = {
            "label": map_activity[activity],
            "user": subjects[user -1],
            "position": "front pocket",
            "device": "iPhone 6S",
            "accelerometer": get_array_of_obj_xyz(accelerometer),
            "gyroscope": get_array_of_obj_xyz(gyroscope)
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "MS")
        os.chdir(path)

        config = self.get_config()

        sub_info = open('data/data_subjects_info.csv', 'r').readlines()
        sub_info = sub_info[1:]
        for users in sub_info:
            user = users.replace("\n", "").split(",")
            if int(user[4]) == 0:
                s = "female"
            else:
                s = "male"
            obj = {
                "id": int(user[0]),
                "age": int(user[3]),
                "height": int(user[2]),
                "weight": int(user[1]),
                "sex": s,
            }
            subjects.append(obj)

        acc_zip = zipfile.ZipFile('data/B_Accelerometer_data.zip')
        gyr_zip = zipfile.ZipFile('data/C_Gyroscope_data.zip')
        for acts in activities:
            for subject in range(1, 25):
                acc = acc_zip.open('B_Accelerometer_data/' + acts + '/sub_' + f'{subject}' + '.csv').readlines()
                gyro = gyr_zip.open('C_Gyroscope_data/' + acts + '/sub_' + f'{subject}' + '.csv').readlines()
                self.create_json(acts, subject, acc, gyro)

        return self.data, config
