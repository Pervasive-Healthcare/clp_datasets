import os
from Utils.driver import Driver

devices = {
    "samsung-samsung-SM-G900F-5.0": "Samsung S5",
    "LGE-lge-LG-H815-5.1": "LG G4"
}

map_activity = {
    "Walking": "walking",
    "Jogging": "jogging",
    "Bending": "bending",
    "Aplausing": "aplausing",
    "Hopping": "hopping",
    "HandsUp": "hands up",
    "GoUpstairs": "go upstairs",
    "GoDownstairs": "go downstairs",
    "MakingACall": "making a call",
    "OpeningDoor": "opening door",
    "LyingDown_OnABed": "lying down on a bed",
    "Sitting_GettingUpOnAChair": "sitting down and getting up from a chair",
    "backwardFall": "fall backwards",
    "forwardFall": "fall forward",
    "lateralFall": "fall lateral"
}

def get_user_obj(user):
    obj = {
        "id": int(user[0].split(" ")[2][8:10]),
        "age": int(user[1].split(" ")[2]),
        "height": int(user[2].split(" ")[2]),
        "weight": int(user[3].split(" ")[2]),
        "sex": user[4].split(" ")[2],
    }
    return obj

def get_array_of_obj_xyz(data):
    array = []
    for single_data in data:
        values = []
        for xyz in single_data[2:5]:
            value = xyz.replace(".", "")
            if value == "-6,10E+10":
                value = "0"
                values.append(value)
                continue
            if value[0] == '-':
                value = value[:2] + "." + value[2:]
            else:
                value = value[:1] + "." + value[1:]
            values.append(value)
        obj = {"timestamp": float(single_data[0]), "x": float(values[0]), "y": float(values[1]), "z": float(values[2])}
        array.append(obj)
    return array

class DriverImplementation(Driver):
    data = []

    def get_dataset_name(self):
        return "UMA"

    def create_json(self, activity, subject, device, accelerometer):
        single_data = {
            "label": map_activity[activity],
            "user": get_user_obj(subject),
            "position": "right pocket",
            "device": devices[device],
            "accelerometer": get_array_of_obj_xyz(accelerometer)
        }
        self.data.append(single_data)

    def read(self, path):
        path = os.path.join(path, "UMA", "")
        os.chdir(path)

        config = self.get_config()

        for _, _, files in os.walk(os.getcwd()):
            for file in files:
                if file.endswith(".csv"):
                    file = open(file, "r")
                    rows = file.readlines()
                    acc = []
                    for row in rows:
                        acc.append(row.replace("\n", "").replace("%", ""))
                    activity = acc[11].split(" ")[5]
                    user = acc[3:8]
                    device = acc[16].split(" ")[3]
                    accelerometer = []
                    for i in range(41, len(acc)):
                        line = acc[i].split(";")
                        if line[5] == "0" and line[6] == "0":
                            accelerometer.append(line)

                    self.create_json(activity, user, device, accelerometer)

        return self.data, config
