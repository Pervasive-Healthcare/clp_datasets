This publicly open data set contais accelerometer data from 8 study subjects performing 5 daily activities which
are labeled followly:

Class label 1 = idling (=sitting/standing)
Class label 2 = walking
Class label 5 = cycling
Class label 6 = driving a car
Class label 7 = running
Class label 0 = other data that is not labeled, may contain sequences of walking, running, cycling and idling. Remove before training classifiers!

Data is collected using Nokia N8 smartphones and the sampling frequency is 40Hz.
Each file contains raw data from one person. File has four columns: 

x-axis accelerometer; y-axis accelerometer; z-axis accelerometer; class label;

Timestamps are not included, but as the frequency is 40Hz it can be concluded that the time difference between two adjacent samples is 25ms.
More details about the data set and study subjects can be found from the article below.
Please, cite to this article when you use this data set in your studies.

Siirtola, P., & R�ning, J. (2012). 
Recognizing Human Activities User-independently on Smartphones Based on Accelerometer Data.
International Journal of Interactive Multimedia and Artificial Intelligence, 1(5).

@article{siirtola2012,
  title={Recognizing Human Activities User-independently on Smartphones Based on Accelerometer Data},
  author={Siirtola, Pekka and R{\"o}ning, Juha},
  journal={International Journal of Interactive Multimedia and Artificial Intelligence},
  volume={1},
  number={5},
  year={2012}
}

If you have any questions or comments, send a message: pekka.siirtola@oulu.fi