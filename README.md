# CLP Datasets

Here are the datasets we used for our research work. For each dataset we developed its driver and collected its support information. 

| Dataset | Sensor | Unit of measure | Frequency (Hz) |
| ------ | ------ | ------ | ------ |
| **[MobiAct](https://bmi.teicrete.gr/en/the-mobifall-and-mobiact-datasets-2/)** | accelerometer (including gravity) | m/s^2 | - |    {% MobiAct %}
|  | gyroscope | rad/s | - |
|  | orientation | degrees | - |
|  |  |  |  |
| **[HAPT](http://archive.ics.uci.edu/ml/datasets/Smartphone-Based%20Recognition%20of%20Human%20Activities%20and%20Postural%20Transitions)** | accelerometer (excluding gravity) | g | 50 |    {% HAPT %}
|  | gyroscope | rad/s | 50 |
|  |  |  |  |
| **[SAD](https://www.utwente.nl/en/eemcs/ps/research/dataset/)** | accelerometer (including gravity) | m/s^2 | 50 |    {% SAD %}
|  | gyroscope | rad/s | 50 |
|  | magnetometer | microTesla | 50 |
|  |  |  |  |
| **[MS](https://github.com/mmalekzadeh/motion-sense)** | accelerometer | g | 50 |    {% MS %}
|  | gyroscope | rad/s | 50 |
|  |  |  |  |
| **[HHAR](https://archive.ics.uci.edu/ml/datasets/Heterogeneity+Activity+Recognition)** | accelerometer | m/s^2 | - |    {% HHAR %}
|  | gyroscope | rad/s | - |
|  |  |  |  |
| **[PARDUSS](https://www.utwente.nl/en/eemcs/ps/research/dataset/)** | accelerometer | m/s^2 | 50 |    {% PARDUSS %}
|  | gyroscope | rad/s | 50 |
|  | magnetometer | microTesla | 50 |
|  |  |  |  |
| **[RW](https://sensor.informatik.uni-mannheim.de/#dataset_realworld)** | accelerometer | m/s^2 | 50 |    {% RW %}
|  | gyroscope | degree/s | 50 |
|  | magnetometer | n.s | 50 |
|  |  |  |  |
| **[UMA](https://figshare.com/articles/UMA_ADL_FALL_Dataset_zip/4214283)** | accelerometer | g | 200 |    {% UMA %}
|  |  |  |  |
| **[UniMiB-SHAR](http://www.sal.disco.unimib.it/technologies/unimib-shar/)** | accelerometer | m/s^2 | 50 |    {% UniMiB-SHAR %}
|  |  |  |  |


# Add a new dataset

1. Download the dataset you want to add.

2. Find on dataset's paper and/or websites the **support information** and put them in a file called 'config.json' inside the dataset folder. The shema of the file 'config.json' is the following:
```
        {
          "properties": {
                "db_name" : {
                  "type": "string"
                },  
                "sensors": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                        "name": {
                            "description": "Name of the sensor",
                            "type": "string"
                        },
                        "uom": {
                            "description": "Unit of measure of the sensor",
                            "type": "string"
                        },
                        "frequency": {
                            "description": "Frequency of the sensor",
                            "type": "integer"
                        }
                        },
                        "required": [
                        "name",
                        "uom"
                        ]
                    }
                },
                "activities": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },

                "required": [
                    "db_name",
                    "sensors",
                    "activities"
                    ]
            }
        }
```

3. Develop the **driver** in Python language. A common interface has been implemented containing some methods that must necessarily be present in the driver:
    - **read(\<path\>)** : main function of the driver that deals with the data processing phase. The \<path\> variable represents the path of the dataset to be imported, which is located inside the local component repository;
    - **getConfig()** : function that returns a dictionary containing the general information of the dataset;
    - **getDatasetName()** : function that returns the dataset name.

     The data schema is:
```
            {
                "definitions": {
                    "sensor_obj": {
                        "type": "array",
                        "item": {
                            "type": "object",
                            "properties": {
                                "timestamp": {
                                    "type": "double"
                                },
                                "x": {
                                    "type": "double"
                                },
                                "y": {
                                    "type": "double"
                                },
                                "z": {
                                    "type": "double"
                                }
                            },
                            "required": [
                                "x", "y", "z"
                            ]
                        }
                    }
                },
                
                "title": "Data",
                "description": "Structure of data json",
                "type": "array",
                "items": {
                    "title": "Data",
                    "description": "Structure of data json",
                    "type": "object",
                    "properties": {
                    "label": {
                        "type": "string"
                    },
                    "user": {
                        "description": "User details",
                        "type": "object",
                        "properties": {
                        "id": {
                            "description": "Id of user into the dataset",
                            "type": "integer"
                        },
                        "age": {
                            "description": "Age of user",
                            "type": "integer"
                        },
                        "height": {
                            "description": "Height of user in cm",
                            "type": "integer"
                        },
                        "weight": {
                            "description": "Weight of user in Kg",
                            "type": "integer"
                        }
                        },
                        "required": [
                        "age",
                        "height",
                        "weight"
                        ]
                    },
                    "exp": {
                        "type": "integer"
                    },
                    "position": {
                        "type": "string"
                    },
                    "device": {
                        "type": "string"
                    },
                    "accelerometer": {
                        "$ref": "#/definitions/sensor_obj"
                    },
                    "gyroscope": {
                        "$ref": "#/definitions/sensor_obj"
                    },
                    "orientation": {
                        "$ref": "#/definitions/sensor_obj"
                    }
                    },
                    "required": [
                    "label"
                    ]
                }
            }
```


 



